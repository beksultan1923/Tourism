//
//  SplashController.swift
//  Tourism
//
//  Created by Eldar Akkozov on 6/7/23.
//

import UIKit

protocol SplashViewDelegate: AnyObject {
    
}

class SplashController: BaseController {
    
    var viewModel: SplashDelagate? = nil
    
    override func viewDidLoad() {
        view.backgroundColor = .red
        
        viewModel?.nextNavigation()
    }
}

extension SplashController: SplashViewDelegate {
    
}
