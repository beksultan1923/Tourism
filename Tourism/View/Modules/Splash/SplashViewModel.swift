//
//  SplashViewModel.swift
//  Tourism
//
//  Created by Eldar Akkozov on 6/7/23.
//

import Foundation

protocol SplashDelagate {
    func nextNavigation()
}

class SplashViewModel: BaseViewModel<SplashRouterDelegate, SplashViewDelegate> {
    
}

extension SplashViewModel: SplashDelagate {
    func nextNavigation() {
        router.showMain()
    }
}
