//
//  SplashBuilder.swift
//  Tourism
//
//  Created by Eldar Akkozov on 6/7/23.
//

import UIKit

class SplashBuilder {
    
    static func create(router: Router) -> UIViewController {
        let controller = SplashController()
        let viewModel = SplashViewModel(router: router, delegate: controller)
        
        controller.viewModel = viewModel
        
        return controller
    }
}
