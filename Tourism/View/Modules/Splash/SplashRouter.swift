//
//  SplashRouter.swift
//  Tourism
//
//  Created by Eldar Akkozov on 6/7/23.
//

import Foundation

protocol SplashRouterDelegate {
    func showMain()
    func showRegister()
    func showOnBording()
}

typealias SplashRouter = SplashRouterDelegate & Router

extension Router: SplashRouterDelegate {
    func showMain() {
        let module = TabBarController()
        
        navigationController.setViewControllers([module], animated: true)
    }
    
    func showRegister() {
        
    }
    
    func showOnBording() {
        
    }
}
