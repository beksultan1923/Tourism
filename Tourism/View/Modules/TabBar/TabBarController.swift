//
//  MainTabBarController.swift
//  Tourism
//
//  Created by Бексултан Мусаев on 6/7/23.
//

import UIKit
import SnapKit

class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        configureVC()
    }
    
    private func configureVC() {
        let mainVC = templateNavigationViewController(MainViewScreen(), unselectedImage: #imageLiteral(resourceName: "mainView"), selectedImage: #imageLiteral(resourceName: "mainView"), title: "Main view")
        let menuVC = templateNavigationViewController(MenuScreen(), unselectedImage: #imageLiteral(resourceName: "menu"), selectedImage: #imageLiteral(resourceName: "menu"), title: "Menu")
        let searchVC = templateNavigationViewController(SearchScreen(), unselectedImage: #imageLiteral(resourceName: "search"), selectedImage: #imageLiteral(resourceName: "search"),  title: "Search")
        let mapVC = templateNavigationViewController(MapScreen(), unselectedImage: #imageLiteral(resourceName: "map"), selectedImage: #imageLiteral(resourceName: "map"),  title: "Map")
        
        
        self.tabBar.tintColor = .tabIem
        self.tabBar.backgroundColor = .white
        self.viewControllers = [mainVC, menuVC, searchVC, mapVC]
    }
    
    // MARK: - Helpers

    private func templateNavigationViewController(_ viewController: UIViewController, unselectedImage: UIImage, selectedImage: UIImage, title: String) -> UINavigationController {
        let nav = UINavigationController(rootViewController: viewController)
        
        nav.tabBarItem.image = unselectedImage
        nav.tabBarItem.selectedImage = selectedImage
        nav.tabBarItem.title = title
        
        nav.navigationBar.shadowImage = UIImage()
        nav.view.backgroundColor = .main
        nav.navigationBar.isTranslucent = true
        
        return nav
    }
}
