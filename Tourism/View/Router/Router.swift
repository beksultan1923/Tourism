//
//  Router.swift
//  Tourism
//
//  Created by Eldar Akkozov on 6/7/23.
//

import UIKit

protocol RouterMain {
    var navigationController: UINavigationController { get set }
    
    init(navigationController: UINavigationController)
}

protocol RouterProtocol: RouterMain  {
    func initialSplashViewContrller()
}

class Router: RouterProtocol {
    
    var navigationController: UINavigationController
    
    required init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func initialSplashViewContrller() {
        let module = SplashBuilder.create(router: self)
        
        navigationController.pushViewController(module, animated: true)
    }
}
