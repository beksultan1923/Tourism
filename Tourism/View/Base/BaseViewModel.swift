//
//  BaseViewModel.swift
//  Tourism
//
//  Created by Eldar Akkozov on 6/7/23.
//

import Foundation

class BaseViewModel<Router, Delegate> {
    
    var router: Router
    var delegate: Delegate
    
    init(router: Router, delegate: Delegate) {
        self.router = router
        self.delegate = delegate
    }
}
