//
//  BaseNavigationController.swift
//  Tourism
//
//  Created by Eldar Akkozov on 6/7/23.
//

import Foundation
import UIKit

class BaseNavigationController: UINavigationController {
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        setUpNavBar()
        setUpUI()
    }
    
    func setUpUI() {
        view.backgroundColor = .white
    }
    
    func setUpNavBar() {
        navigationBar.backgroundColor = UIColor.white
        navigationBar.isTranslucent = false
        
        isNavigationBarHidden = true
    }
}
