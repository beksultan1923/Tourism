//
//  AppDelegate.swift
//  Tourism
//
//  Created by Eldar Akkozov on 5/7/23.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    static var shared: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        regularLaunching()
        
        return true
    }
    
    func regularLaunching() {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.overrideUserInterfaceStyle = .light
               
        let navigationController = BaseNavigationController()
        let router = Router(navigationController: navigationController)

        router.initialSplashViewContrller()
        
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
    }
}

