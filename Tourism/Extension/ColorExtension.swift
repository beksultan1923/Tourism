//
//  ColorExtension.swift
//  Tourism
//
//  Created by Бексултан Мусаев on 6/7/23.
//

import Foundation
import UIKit

extension UIColor {
    
    static var tabColor = UIColor(hex: "#EFEBE7")

    static var tabIem = UIColor(hex: "#171D20")
//    /// hex color #00FF00
    static var main = UIColor(hex: "#EFEBE7")
//    /// hex color #FFFFFF
//    static var newdioWhite = UIColor(hex: "#FFFFFF")
//    /// hex color #BBBBBB
//    static var newdioGray1 = UIColor(hex: "#BBBBBB")
//    /// hex color #919191
//    static var newdioGray2 = UIColor(hex: "#919191")
//    /// hex color #303030
//    static var newdioGray3 = UIColor(hex: "#303030")
//    /// hex color #202020
//    static var newdioGray4 = UIColor(hex: "#202020")
//    /// hex color #171717
//    static var newdioGray5 = UIColor(hex: "#171717")
//    /// hex color #2A2A2A
//    static var newdioGray6 = UIColor(hex: "#2A2A2A")
//    /// hex color #000000
//    static var newdioBlack = UIColor.newdioGray5
//    /// hex color #52B5F9
//    static var newdioSkyblue = UIColor(hex: "#52B5F9")
//    /// hex color #03C75A
//    static var naver = UIColor(hex: "#03C75A")
//    /// hex color #F8E318
//    static var kakao = UIColor(hex: "#F8E318")
//    /// hex color #FFFFFF
//    static var apple = UIColor(hex: "#FFFFFF")
//    /// hex color #FFFFFF
//    static var google = UIColor(hex: "#FFFFFF")
    
    convenience init(hex: String) {
        let scanner = Scanner(string: hex)
        _ = scanner.scanString("#")
        
        var rgb: UInt64 = 0
        scanner.scanHexInt64(&rgb)
        
        let r = Double((rgb >> 16) & 0xFF) / 255.0
        let g = Double((rgb >> 8) & 0xFF) / 255.0
        let b = Double((rgb >> 0) & 0xFF) / 255.0
        self.init(red: CGFloat(r), green: CGFloat(g), blue: CGFloat(b), alpha: 1)
    }
}

